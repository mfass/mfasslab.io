---
layout: post
title: "My Research Frameworks"
---

My research to date has been primarily outside of both frameworks discussed in the external readings, but both contain examples relevant to my previous research. One of my previous publications could be considered to be within the SES (social-ecological system) framework, despite my attempt at the time to write a strictly ecological literature review. 

Two-Eyed Seeing is not a framework that I have previously used to situate my research, despite having conducted my masters' work on the shores of Mi'kma'ki. I intend to implement it as I begin my work with the BC Kelp Resilience project and Haida Nation. My first steps in initiating this project, currently on my to-do list, is to contact our collaborators in Haida Gwaii to begin the application principles outlined in the reading[^1]; create an authentic relationship between myself as a settler-researcher and the community I will be depending on, identify places of potential reciprocity in research and labor, and to begin planning out the most fulfilling ways to involve the community. 

My previous research system was one that mirrored, in both location, extent, and community, the Maine lobster community conflicts outlined in the SES reading[^2]. My study seaweed, _Ascophyllum nodosum_, is primarily managed by its harvest industry in Nova Scotia, in ways that mirror the local lobster industry. In my first single author paper[^3], I devote a large amount of page space to describing some of the economic issues at play that shape current management of the harvest regime. As Orstom states in the reading, natural and social systems are deeply intertwined. As the economic aspect is part of that social system, it is interesting to me how impossible it was to disentangle the phenomenon I was describing in my masters work from the socio-economic factors driving the changes. 

[^1]: Wright, A.L., Gabel, C., Ballantyne, M., Jack, S.M., and Wahoush, O. 2019. Using Two-Eyed Seeing in Research With Indigenous People: An Integrative Review. International Journal of Qualitative Methods 18: 160940691986969. doi:10.1177/1609406919869695.

[^2]: Ostrom, E. 2007. A diagnostic approach for going beyond panaceas. Proceedings of the National Academy of Sciences 104(39): 15181–15187. Proceedings of the National Academy of Sciences. doi:10.1073/pnas.0702288104.

[^3]: Fass, M.P. 2021. The current rockweed, Ascophyllum nodosum, harvesting regime on the shores of Nova Scotia - a review. Proceedings of the Nova Scotian Institute of Science 51(2): 443–454. doi:10.15273/pnsis.v51i2.11166.
