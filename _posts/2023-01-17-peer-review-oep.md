---
layout: post
title: "Ontologies, Epistemologies, and Paradigms in _Historial distribution of kelp forests on the coast of British Columbia: 1858-1956._"
---
For situating a relevant peer-reviewed paper in the context of ontologies, epistemologies, and paradigms, I selected _Historical distribution of kelp forests on the coast of British Columbia: 1858-1956_.[^1] This paper, lead by my supervisor Dr. Maycira Costa, is foundational to the work I intend to achieve during my doctoral studies. 

This paper is entirely situated within the paradigm of spatial science -- the important of kelp in a more holistic sense to humans and broader systems is addressed in the introduction, but is not the focus of the paper. 

In a possible departure from broader spatial science ontologies, this piece is deliberately and specifically attempting to link space and time, across the binary boundary proposed by dualist ontologies. While not explicitly monist in the reasoning behind the study, the deliberate hybridization of spatial data with time series data merits mention. 

As a methodological paper, _Historical distributions_ is not particularly concerned with causality of time on kelp distribution. However, the larger body of literature it is situated within views kelp distribution as a complex discrete causality, impacted by a variety of factors and with measurable impacts. 

Epistemologically it is harder to place this paper. If anything, it sidesteps epistemology completely as a broader concept and zeroes in on the specific nautical charts assembled in the study. The categorization of kelps on historial charts is certainly considered, as is the connections between data on one sheet and another. At this microscale, I struggle to connect the methods here with a broader range of philosophical thought. The paper positions not the individual now, but the individual _then_ as the knowing agent, which I find deeply interesting. 



[^1]: Costa, M., Le Baron, N., Tenhunen, K., Nephin, J., Willis, P., Mortimor, J.P., Dudas, S., and Rubidge, E. 2020. Historical distribution of kelp forests on the coast of British Columbia: 1858–1956. Applied Geography 120: 102230. doi:10.1016/j.apgeog.2020.102230.
