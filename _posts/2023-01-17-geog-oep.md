---
layout: post
title: "Ontologies, Epistemologies, and Paradigms in Geography"
---

Ontology (noun); a set of assumptions and theories that explore what the world is like. 

Epistemology (noun): the theory of knowledge -- the reasoning behind categorization of phenomena in the world, and how connections between differing phenomenta are understood. 

Paradigm (noun): a model or pattern of thought and reasoning; the body of literature that shares fundamental assumptions about the world, ways of researching the world, and what aspects of the world are important to study. 

In the interdisciplenary and extremely varied field of Geography, ontologies, epistemologies, and paradigms are also varied among sub-fields. Across the physical-human geography spectrum, however, all three come back, again and again, to the *world*. Geographers as a whole are concerned with *place*, though the specific aspects of place might vary from a human geographer looking at geographies of interpersonal violence and a spatial geographer concerned with migratory birds. My definitions above are all centered on the *world*, Earth, as the macro*place* geographers are primary concerned with[^1]. 

Ontologies in geography seem to tend towards a dualist philosophy -- the divide between physical and human geography seems to lend itself to a dualist position. The binary created between the physical world (as if that is not influenced by humans) and the human world (as if it is not shaped by place) appears to me to be reductive and deliberately shaped by older epistemologies. I particularly suspect epistemologies of the mind-as-knowing-agents as described in Gomez and Jones[^2] for their role in shaping the physical-human geography divide and perpetuating a hard/soft science binary. Movements like feminist geographies that discard the persona of the 'objective scientist' seem more in line with Geography as an entire discipline. It also appears more in touch with the (monist?) stance I hold that Planet Earth is a vastly complex interconnected system, where social phenomena are impossible to divorce from those in nature.

[^1]: My understanding is that xeno-geography is at best a nascent field; please correct me if I am wrong! 
[^2]: Gomez, B., and Jones, J.P. (Editors). 2010. Research methods in geography: a critical introduction. Wiley-Blackwell, Chichester, West Sussex, U.K. ; Malden, MA.
