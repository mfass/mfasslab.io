---
layout: home
title: "Home"
---

Megan P. Fass is a PhD student at the [SPECTRAL](link) laboratory at the University of Victoria. They are deeply interested in the ways humans relate to and utilize brown seaweeds in their local coastlines. Their research to date focuses on tracking changes to the bounds of seaweed beds driven by anthropogenic change using remote sensing technologies. 

This website is generated as part of coursework for GEOG 500B at the University of Victoria. Certain blog posts and features of the site will be under construction throughout early 2023. 

